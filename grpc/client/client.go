package client

import (
	"order_service/genproto/order_service"
	pbu "order_service/genproto/user_service"

	"order_service/config"

	"google.golang.org/grpc"
)

type IServiceManager interface {
	OrderService() order_service.OrderServiceClient
	OrderProductsService() order_service.OrderProductsServiceClient
	DeliveryTarifService() order_service.DeliveryTarifServiceClient
	ALternativeTarifService() order_service.AlternativeTarifServiceClient

	BranchService() pbu.BranchServiceClient
	ClientService() pbu.ClientServiceClient
}

type grpcClients struct {
	orderService            order_service.OrderServiceClient
	orderProductsService    order_service.OrderProductsServiceClient
	deliveryTarifService    order_service.DeliveryTarifServiceClient
	alternativeTarifService order_service.AlternativeTarifServiceClient
	branchService           pbu.BranchServiceClient
	clientService           pbu.ClientServiceClient
}

func NewGrpcClients(cfg config.Config) (IServiceManager, error) {
	connOrderService, err := grpc.Dial(
		"localhost:8086",
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	connUserClients, err := grpc.Dial(
		"localhost:8003",
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		orderService:            order_service.NewOrderServiceClient(connOrderService),
		orderProductsService:    order_service.NewOrderProductsServiceClient(connOrderService),
		deliveryTarifService:    order_service.NewDeliveryTarifServiceClient(connOrderService),
		alternativeTarifService: order_service.NewAlternativeTarifServiceClient(connOrderService),
		branchService:           pbu.NewBranchServiceClient(connUserClients),
		clientService:           pbu.NewClientServiceClient(connUserClients),
	}, nil
}

func (g *grpcClients) OrderService() order_service.OrderServiceClient {
	return g.orderService
}
func (g *grpcClients) OrderProductsService() order_service.OrderProductsServiceClient {
	return g.orderProductsService
}
func (g *grpcClients) DeliveryTarifService() order_service.DeliveryTarifServiceClient {
	return g.deliveryTarifService
}
func (g *grpcClients) ALternativeTarifService() order_service.AlternativeTarifServiceClient {
	return g.alternativeTarifService
}
func (g *grpcClients) BranchService() pbu.BranchServiceClient {
	return g.branchService
}
func (g *grpcClients) ClientService() pbu.ClientServiceClient {
	return g.clientService
}