package grpc

import (
	pbo "order_service/genproto/order_service"
	"order_service/grpc/client"
	"order_service/pkg/logger"
	"order_service/service"
	"order_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *grpc.Server {
	grpcServer := grpc.NewServer()

	pbo.RegisterOrderServiceServer(grpcServer, service.NewOrderService(strg, services, log))
	pbo.RegisterOrderProductsServiceServer(grpcServer, service.NewOrderProductsService(strg, services, log))
	pbo.RegisterAlternativeTarifServiceServer(grpcServer, service.NewAlternativeTarifService(strg, services, log))
	pbo.RegisterDeliveryTarifServiceServer(grpcServer, service.NewDeliveryTarifService(strg, services, log))
	
	reflection.Register(grpcServer)

	return grpcServer
}
