package service

import (
	"context"
	"fmt"
	pbo "order_service/genproto/order_service"
	pbu "order_service/genproto/user_service"
	"order_service/grpc/client"
	"order_service/pkg/logger"
	"order_service/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type orderProductsService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewOrderProductsService(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *orderProductsService {
	return &orderProductsService{
		storage:  strg,
		services: services,
		log:      log,
	}
}

func (o *orderProductsService) Create(ctx context.Context, request *pbo.CreateOrderProducts) (*pbo.OrderProducts, error) {
	id, err := o.services.OrderService().Get(context.Background(), &pbo.PrimaryKeyOrder{Id: request.OrderId})
	if err != nil {
		fmt.Println("err", err)
	}
	idclient, err := o.services.ClientService().Get(context.Background(), &pbu.PrimaryKeyClient{Id: id.ClientId})
	if err != nil {
		fmt.Println("err", err)
	}
	updatedclient, err := o.services.ClientService().Update(context.Background(), &pbu.Client{
		Id:               idclient.Id,
		FirstName:        idclient.FirstName,
		LastName:         idclient.LastName,
		Phone:            idclient.Phone,
		LastOrderedDate:  idclient.CreatedAt,
		TotalOrdersCount: idclient.TotalOrdersCount + 1,
		TotalOrdersSum:   (request.Price * float32(request.Quantity)) - idclient.DiscountAmount,
	})
	if err != nil {
		fmt.Println("err", err)
	}
	fmt.Println("updatedclient", updatedclient)
	return o.storage.OrderProducts().Create(ctx, request)
}
func (o *orderProductsService) Get(ctx context.Context, request *pbo.OrderProductsPrimaryKey) (*pbo.OrderProducts, error) {
	return o.storage.OrderProducts().Get(ctx, request)
}
func (o *orderProductsService) GetList(ctx context.Context, request *pbo.OrderProductsRequest) (*pbo.OrderProductsResponse, error) {
	return o.storage.OrderProducts().GetList(ctx, request)
}
func (o *orderProductsService) Update(ctx context.Context, request *pbo.OrderProducts) (*pbo.OrderProducts, error) {
	return o.storage.OrderProducts().Update(ctx, request)
}
func (o *orderProductsService) Delete(ctx context.Context, request *pbo.OrderProductsPrimaryKey) (*emptypb.Empty, error) {
	return o.storage.OrderProducts().Delete(ctx, request)
}
