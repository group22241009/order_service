package service

import (
	"context"
	pbo "order_service/genproto/order_service"
	"order_service/grpc/client"
	"order_service/pkg/logger"
	"order_service/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type alternativeTarifService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewAlternativeTarifService(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *alternativeTarifService {
	return &alternativeTarifService{
		storage:  strg,
		services: services,
		log:      log,
	}
}
func (a *alternativeTarifService) Create(ctx context.Context, request *pbo.CreateAlternativeTarif) (*pbo.AlternativeTarif, error) {
	return a.storage.AlternativeTarif().Create(ctx, request)
}
func (a *alternativeTarifService) Get(ctx context.Context, request *pbo.AlternativeTarifPrimaryKey) (*pbo.AlternativeTarif, error) {
	return a.storage.AlternativeTarif().Get(ctx, request)
}
func (a *alternativeTarifService) GetList(ctx context.Context, request *pbo.AlternativeTRequest) (*pbo.AlternativeTResponse, error) {
	return a.storage.AlternativeTarif().GetList(ctx, request)
}
func (a *alternativeTarifService) Update(ctx context.Context, request *pbo.AlternativeTarif) (*pbo.AlternativeTarif, error) {
	return a.storage.AlternativeTarif().Update(ctx, request)
}
func (a *alternativeTarifService) Delete(ctx context.Context, request *pbo.AlternativeTarifPrimaryKey) (*emptypb.Empty, error) {
	return a.storage.AlternativeTarif().Delete(ctx, request)
}
