package service

import (
	"context"
	pbo "order_service/genproto/order_service"
	"order_service/grpc/client"
	"order_service/pkg/logger"
	"order_service/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type deliveryTarifService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewDeliveryTarifService(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *deliveryTarifService {
	return &deliveryTarifService{
		storage:  strg,
		services: services,
		log:      log,
	}
}
func (d *deliveryTarifService) Create(ctx context.Context, request *pbo.CreateDeliveryTarif) (*pbo.DeliveryTarif, error) {
	return d.storage.DeliveryTarif().Create(ctx, request)
}
func (d *deliveryTarifService) Get(ctx context.Context, request *pbo.PrimaryKeyDelivery) (*pbo.DeliveryTarif, error) {
	return d.storage.DeliveryTarif().Get(ctx, request)
}
func (d *deliveryTarifService) GetList(ctx context.Context, request *pbo.GetListRequestDelivery) (*pbo.DeliveryTarifResponse, error) {
	return d.storage.DeliveryTarif().GetList(ctx, request)
}
func (d *deliveryTarifService) Update(ctx context.Context, request *pbo.DeliveryTarif) (*pbo.DeliveryTarif, error) {
	return d.storage.DeliveryTarif().Update(ctx, request)
}
func (d *deliveryTarifService) Delete(ctx context.Context, request *pbo.PrimaryKeyDelivery) (*emptypb.Empty, error) {
	return d.storage.DeliveryTarif().Delete(ctx, request)
}
