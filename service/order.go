package service

import (
	"context"
	"fmt"
	pbo "order_service/genproto/order_service"
	pbu "order_service/genproto/user_service"
	"order_service/grpc/client"
	"order_service/pkg/check"
	"order_service/pkg/logger"
	"order_service/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type orderService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewOrderService(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *orderService {
	return &orderService{
		storage:  strg,
		services: services,
		log:      log,
	}
}
func (o *orderService) Create(ctx context.Context, request *pbo.CreateOrder) (*pbo.Order, error) {
	idcount, err := o.storage.Orders().Count(context.Background(), &emptypb.Empty{})
	if err != nil {
		fmt.Println("error is while counting last desc of orders id", err)
	}
	branch, err := o.services.BranchService().Get(context.Background(), &pbu.PrimaryKeyBranch{
		Id: request.BranchId,
	})
	if err != nil {
		o.log.Error("error", logger.Error(err))
	}
	idstr := check.GenerateExternalID(idcount.Id, branch.Name)
	fmt.Println("------------------")

	getdeliverytarif, err := o.services.DeliveryTarifService().Get(context.Background(), &pbo.PrimaryKeyDelivery{Id: branch.DeliveryTarif})
	if err != nil {
		fmt.Println("err++++++++++", err)
	}
	if getdeliverytarif.Type == "fixed" {
		request.Price += getdeliverytarif.BasePrice
	}
	if getdeliverytarif.Type == "alternative" {
		alternatives, err := o.services.ALternativeTarifService().GetList(context.Background(), &pbo.AlternativeTRequest{Page: 1, Limit: 10})
		if err != nil {
			fmt.Println("err----------------", err)
		}
		for _, v := range alternatives.AlternativeTarifs {
			if request.Price >= v.FromPrice && request.Price <= v.ToPrice {
				request.Price += v.Price
			} else {
				request.Price += 0
			}
		}

	}

	request.Id = idstr
	return o.storage.Orders().Create(ctx, request)
}
func (o *orderService) Get(ctx context.Context, request *pbo.PrimaryKeyOrder) (*pbo.Order, error) {
	return o.storage.Orders().Get(ctx, request)
}
func (o *orderService) GetList(ctx context.Context, request *pbo.GetListRequestOrder) (*pbo.OrderListResponse, error) {
	return o.storage.Orders().GetList(ctx, request)
}
func (o *orderService) Update(ctx context.Context, request *pbo.Order) (*pbo.Order, error) {
	return o.storage.Orders().Update(ctx, request)
}
func (o *orderService) Delete(ctx context.Context, request *pbo.PrimaryKeyOrder) (*emptypb.Empty, error) {
	return o.storage.Orders().Delete(ctx, request)
}
func (o *orderService) Count(ctx context.Context, e *emptypb.Empty) (*pbo.OrderId, error) {
	return o.storage.Orders().Count(ctx, e)
}
