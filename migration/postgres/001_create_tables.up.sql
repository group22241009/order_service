CREATE TYPE order_type AS ENUM (
  'delivery',
  'pick_up'
);

CREATE TYPE delivery_tarif_type AS ENUM (
  'fixed',
  'alternative'
);

CREATE TYPE status_type AS ENUM (
  'accepted',
  'courier_accepted',
  'ready_in_branch',
  'on_way',
  'finished',
  'canceled'
);


CREATE TYPE payment_type AS ENUM (
  'cash',
  'card'
);

CREATE TABLE IF NOT EXISTS orders (
  id uuid PRIMARY KEY,
  client_id uuid,
  branch_id uuid,
  type order_type,
  address varchar(20),
  courier_id uuid,
  price float,
  delivery_price float,
  discount float,
  status status_type,
  payment_type payment_type,
  created_at timestamp default now(),
  updated_at timestamp,
  deleted_at int default 0
);

CREATE TABLE IF NOT EXISTS delivery_tarif (
  id uuid PRIMARY KEY,
  name varchar(20),
  type delivery_tarif_type,
  base_price float,
  created_at timestamp DEFAULT now(),
  updated_at timestamp DEFAULT now(),
  deleted_at int default 0
);

CREATE TABLE IF NOT EXISTS alternative_tarif (
  id uuid PRIMARY KEY,
  from_price float,
  to_price float,
  price float,
  delivery_tarif_id uuid
);

CREATE TABLE IF NOT EXISTS order_products (
  id uuid PRIMARY KEY,
  product_id uuid,
  quantity int,
  price float,
  order_id uuid
);

ALTER TABLE order_products ADD FOREIGN KEY (order_id) REFERENCES orders (id);
ALTER TABLE alternative_tarif ADD FOREIGN KEY (delivery_tarif_id) REFERENCES delivery_tarif (id);
