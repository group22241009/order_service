drop type if exists "order_type";
drop type if exists "delivery_tarif_type";
drop type if exists "status_type";
drop type if exists "values_enum";
drop type if exists "payment_type";

drop table if exists "orders";
drop table if exists "delivery_tarif";
drop table if exists "order_products";
drop table if exists "alternative_tarif";

