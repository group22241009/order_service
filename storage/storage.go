package storage

import (
	"context"
	pbo "order_service/genproto/order_service"
	"google.golang.org/protobuf/types/known/emptypb"
)

type IStorage interface {
	Close()
	Orders() IOrdersStorage
	OrderProducts() IOrderProductsStorage
	DeliveryTarif() IDeliveryTarifStorage
	AlternativeTarif() IAlternativeTarifStorage
}
type IOrdersStorage interface {
	Create(context.Context, *pbo.CreateOrder) (*pbo.Order, error)
	Get(context.Context, *pbo.PrimaryKeyOrder) (*pbo.Order, error)
	GetList(context.Context, *pbo.GetListRequestOrder) (*pbo.OrderListResponse, error)
	Update(context.Context, *pbo.Order) (*pbo.Order, error)
	Delete(context.Context, *pbo.PrimaryKeyOrder) (*emptypb.Empty, error)
	Count(context.Context,*emptypb.Empty)(*pbo.OrderId,error)
}
type IOrderProductsStorage interface {
	Create(context.Context, *pbo.CreateOrderProducts) (*pbo.OrderProducts, error)
	Get(context.Context, *pbo.OrderProductsPrimaryKey) (*pbo.OrderProducts, error)
	GetList(context.Context, *pbo.OrderProductsRequest) (*pbo.OrderProductsResponse, error)
	Update(context.Context, *pbo.OrderProducts) (*pbo.OrderProducts, error)
	Delete(context.Context, *pbo.OrderProductsPrimaryKey) (*emptypb.Empty, error)
}
type IDeliveryTarifStorage interface {
	Create(context.Context, *pbo.CreateDeliveryTarif) (*pbo.DeliveryTarif, error)
	Get(context.Context, *pbo.PrimaryKeyDelivery) (*pbo.DeliveryTarif, error)
	GetList(context.Context, *pbo.GetListRequestDelivery) (*pbo.DeliveryTarifResponse, error)
	Update(context.Context, *pbo.DeliveryTarif) (*pbo.DeliveryTarif, error)
	Delete(context.Context, *pbo.PrimaryKeyDelivery) (*emptypb.Empty, error)
}
type IAlternativeTarifStorage interface{
	Create(context.Context, *pbo.CreateAlternativeTarif) (*pbo.AlternativeTarif, error)
	Get(context.Context, *pbo.AlternativeTarifPrimaryKey) (*pbo.AlternativeTarif, error)
	GetList(context.Context, *pbo.AlternativeTRequest) (*pbo.AlternativeTResponse, error)
	Update(context.Context, *pbo.AlternativeTarif) (*pbo.AlternativeTarif, error)
	Delete(context.Context, *pbo.AlternativeTarifPrimaryKey) (*emptypb.Empty, error)
}