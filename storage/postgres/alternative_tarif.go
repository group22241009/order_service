package postgres

import (
	"context"
	"fmt"
	pbo "order_service/genproto/order_service"
	"order_service/pkg/helper"
	"order_service/pkg/logger"
	"order_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"
)

type alternativeTarifRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewAlternativeTarifRepo(db *pgxpool.Pool, log logger.ILogger) storage.IAlternativeTarifStorage {
	return &alternativeTarifRepo{
		db:  db,
		log: log,
	}
}

func (a *alternativeTarifRepo) Create(ctx context.Context, request *pbo.CreateAlternativeTarif) (*pbo.AlternativeTarif, error) {
	var (
		alternative = pbo.AlternativeTarif{}
		err         error
	)
	query := `insert into alternative_tarif(id,from_price,to_price,price)
	          values($1,$2,$3,$4)
	          returning id,from_price,to_price,price `


	
	if err = a.db.QueryRow(ctx, query, uuid.New().String(), request.GetFromPrice(), request.GetToPrice(), request.GetPrice()).Scan(
		&alternative.Id,
		&alternative.FromPrice,
		&alternative.ToPrice,
		&alternative.Price,
	
	); err != nil {
		fmt.Println("error inserting",err)
	}

	return &alternative, nil

}
func (a *alternativeTarifRepo) Get(ctx context.Context, request *pbo.AlternativeTarifPrimaryKey) (*pbo.AlternativeTarif, error) {

	alternative := pbo.AlternativeTarif{}
	query := ` select id,from_price,to_price,price from alternative_tarif where id=$1`
	if err := a.db.QueryRow(ctx, query, request.GetId()).Scan(
		&alternative.Id,
		&alternative.FromPrice,
		&alternative.ToPrice,
		&alternative.Price,
	
	); err != nil {
		a.log.Error("error getting alternatives", logger.Error(err))
		return nil, err
	}
	return &alternative, nil

}
func (a *alternativeTarifRepo) GetList(ctx context.Context, request *pbo.AlternativeTRequest) (*pbo.AlternativeTResponse, error) {
	var (
		resp   = pbo.AlternativeTResponse{}
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)

	query := fmt.Sprintf("SELECT id, from_price, to_price, price FROM alternative_tarif OFFSET %d LIMIT %d", offset, request.GetLimit())

	rows, err := a.db.Query(ctx, query)
	if err != nil {
		a.log.Error("error while getting", logger.Error(err))
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		alternative := pbo.AlternativeTarif{}

		if err = rows.Scan(
			&alternative.Id,
			&alternative.FromPrice,
			&alternative.ToPrice,
			&alternative.Price,
		); err != nil {
			a.log.Error("error getting list of orders", logger.Error(err))
			return nil, err
		}
		resp.AlternativeTarifs = append(resp.AlternativeTarifs, &alternative)
	}

	resp.Count = count
	return &resp, nil

}
func (a *alternativeTarifRepo) Update(ctx context.Context, request *pbo.AlternativeTarif) (*pbo.AlternativeTarif, error) {
	var (
		alternative = pbo.AlternativeTarif{}
		params      = make(map[string]interface{})
		query       = `update alternative_tarif set `
		filter      = ""
	)

	params["id"] = request.GetId()
	fmt.Println("alternative_tarif_id", request.GetId())

	if request.GetFromPrice() != 0.0 {
		params["from_price"] = request.GetFromPrice()
		filter += " from_price= @from_price,"
	}
	if request.GetToPrice() != 0.0 {
		params["to_price"] = request.GetToPrice()
		filter += " to_price= @to_price,"
	}
	if request.GetPrice() != 0.0 {
		params["price"] = request.GetPrice()
		filter += " price= @price,"
	}
	
	query += filter + ` updated_at = now() where id = @id returning id,from_price,to_price,price,delivery_id `

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := a.db.QueryRow(ctx, fullQuery, args...).Scan(
		&alternative.Id,
		&alternative.FromPrice,
		&alternative.ToPrice,
		&alternative.Price,
	); err != nil {
		return nil, err
	}
	return &alternative, nil

}
func (a *alternativeTarifRepo) Delete(ctx context.Context, request *pbo.AlternativeTarifPrimaryKey) (*emptypb.Empty, error) {

	query := `update alternative_tarif set deleted_at = extract(epoch from current_timestamp) where id = $1`
	_, err := a.db.Exec(ctx, query, request.Id)

	return nil, err
}
