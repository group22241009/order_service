package postgres

import (
	"context"
	"fmt"
	pbo "order_service/genproto/order_service"
	"order_service/pkg/helper"
	"order_service/pkg/logger"
	"order_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"
)

type orderProductsRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewOrderProductsRepo(db *pgxpool.Pool, log logger.ILogger) storage.IOrderProductsStorage {
	return &orderProductsRepo{
		db:  db,
		log: log,
	}
}

func (o *orderProductsRepo) Create(ctx context.Context, request *pbo.CreateOrderProducts) (*pbo.OrderProducts, error) {
	var (
		orderproducts = pbo.OrderProducts{}
		err           error
	)
	query := ` insert into orders(id,product_id,quantity,price,order_id) values($1,$2,$3,$4,$5)
	returning id,product_id,quantity,price,order_id `
	request.OrderId = uuid.New().String()
	request.ProductId = uuid.New().String()
	if err = o.db.QueryRow(ctx, query, uuid.New().String(), request.GetProductId(), request.GetQuantity(), request.GetPrice(), request.GetOrderId()).Scan(
		&orderproducts.Id,
		&orderproducts.ProductId,
		&orderproducts.Quantity,
		&orderproducts.Price,
		&orderproducts.OrderId,
	); err != nil {
		o.log.Error("error", logger.Error(err))
		return nil, err
	}

	return &orderproducts, nil
}
func (o *orderProductsRepo) Get(ctx context.Context, request *pbo.OrderProductsPrimaryKey) (*pbo.OrderProducts, error) {
	orderproducts := pbo.OrderProducts{}
	query := ` select id,product_id,quantity,price,order_id from order_products where deleted_at=0 and id=$1 `
	if err := o.db.QueryRow(ctx, query, request.GetId()).Scan(
		&orderproducts.Id,
		&orderproducts.ProductId,
		&orderproducts.Quantity,
		&orderproducts.Price,
		&orderproducts.OrderId,
	); err != nil {
		o.log.Error("error", logger.Error(err))
		return nil, err
	}
	return &orderproducts, nil

}

func (o *orderProductsRepo) GetList(ctx context.Context, request *pbo.OrderProductsRequest) (*pbo.OrderProductsResponse, error) {
	var (
		resp   = pbo.OrderProductsResponse{}
		filter = " where deleted_at=0"
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)


	countQuery := `select count(*) from order_products ` + filter
	if err := o.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		o.log.Error("error while scanning count of order_products", logger.Error(err))
		return nil, err
	}

	query := ` select id,product_id,quantity,price,order_id from order_products ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())

	rows, err := o.db.Query(ctx, query)
	if err != nil {
		o.log.Error("error while getting order_products rows", logger.Error(err))
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		orderproducts := pbo.OrderProducts{}
		if err = rows.Scan(
			&orderproducts.Id,
			&orderproducts.ProductId,
			&orderproducts.Quantity,
			&orderproducts.Price,
			&orderproducts.OrderId,
		); err != nil {
			o.log.Error("err", logger.Error(err))
			return nil, err
		}
		resp.OrderProducts = append(resp.OrderProducts, &orderproducts)
	}
	resp.Count = count

	return &resp, nil

}
func (o *orderProductsRepo) Update(ctx context.Context, request *pbo.OrderProducts) (*pbo.OrderProducts, error) {
	var (
		orderproducts = pbo.OrderProducts{}
		params        = make(map[string]interface{})
		query         = `update order_products set `
		filter        = ""
	)

	params["id"] = request.GetId()
	fmt.Println("order_products id", request.GetId())

	if request.GetOrderId() != "" {
		params["order_id"] = request.OrderId
		filter += " order_id= @order_id,"
	}
	if request.GetQuantity() != 0 {
		params["qunatity"] = request.GetQuantity()
		filter += " quantity= @quantity,"
	}
	if request.GetPrice() != 0.0 {
		params["price"] = request.GetPrice()
		filter += " price= @price,"
	}
	if request.GetProductId() != "" {
		params["product_id"] = request.GetProductId()
		filter += " product_id= @product_id,"
	}
	
	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id,product_id,quantity,price,order_id  `

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := o.db.QueryRow(ctx, fullQuery, args...).Scan(
		&orderproducts.Id,
		&orderproducts.ProductId,
		&orderproducts.Quantity,
		&orderproducts.Price,
		&orderproducts.OrderId,
	); err != nil {
		o.log.Error("err", logger.Error(err))
		return nil, err
	}

	return &orderproducts, nil

}
func (o *orderProductsRepo) Delete(ctx context.Context, request *pbo.OrderProductsPrimaryKey) (*emptypb.Empty, error) {
	query := `update order_products set deleted_at = extract(epoch from current_timestamp) where id = $1`
	_, err := o.db.Exec(ctx, query, request.Id)

	return nil, err
}
