package postgres

import (
	"context"
	"fmt"
	pbo "order_service/genproto/order_service"
	"order_service/pkg/helper"
	"order_service/pkg/logger"
	"order_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"
)

type deliveryTarifRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewDeliveryTarifRepo(db *pgxpool.Pool, log logger.ILogger) storage.IDeliveryTarifStorage {
	return &deliveryTarifRepo{
		db:  db,
		log: log,
	}
}
func (d *deliveryTarifRepo) Create(ctx context.Context, request *pbo.CreateDeliveryTarif) (*pbo.DeliveryTarif, error) {
	var (
		delivery = pbo.DeliveryTarif{}
		err      error
	)
	query := ` insert into delivery_tarif(id,name,type,base_price,searching_column) 
	          values($1,$2,$3,$4,$5)
			  returning id, name, type, base_price `

	searchingColumn := fmt.Sprintf("%s %f", request.GetName(), request.GetBasePrice())
	if err = d.db.QueryRow(ctx, query, uuid.New().String(), request.GetName(), request.GetType(), request.GetBasePrice(), searchingColumn).Scan(
		&delivery.Id,
		&delivery.Name,
		&delivery.Type,
		&delivery.BasePrice,
	); err != nil {
		fmt.Println("err", err)
	}

	return &delivery, nil

}
func (d *deliveryTarifRepo) Get(ctx context.Context, request *pbo.PrimaryKeyDelivery) (*pbo.DeliveryTarif, error) {
	delivery := pbo.DeliveryTarif{}
	query := ` select id,name,type,base_price from delivery_tarif where deleted_at=0 and id=$1`
	if err := d.db.QueryRow(ctx, query, request.GetId()).Scan(
		&delivery.Id,
		&delivery.Name,
		&delivery.Type,
		&delivery.BasePrice,
	); err != nil {
		fmt.Println("err", err)
	}
	return &delivery, nil

}
func (d *deliveryTarifRepo) GetList(ctx context.Context, request *pbo.GetListRequestDelivery) (*pbo.DeliveryTarifResponse, error) {
	var (
		resp   = pbo.DeliveryTarifResponse{}
		filter = " where deleted_at=0"
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)
	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%s'", request.GetSearch())
	}
	countQuery := `select count(*) from delivery_tarif ` + filter
	if err := d.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		d.log.Error("error while scanning count ", logger.Error(err))
		return nil, err
	}

	query := ` select id,name,type,base_price from delivery_tarif` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())
	rows, err := d.db.Query(ctx, query)
	if err != nil {
		d.log.Error("error while getting", logger.Error(err))
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		delivery := pbo.DeliveryTarif{}

		if err = rows.Scan(
			&delivery.Id,
			&delivery.Name,
			&delivery.Type,
			&delivery.BasePrice,
		); err != nil {
			d.log.Error("error getting list of delivery_tarifs", logger.Error(err))
			return nil, err
		}
		resp.DeliveryTarifs = append(resp.DeliveryTarifs, &delivery)
	}

	resp.Count = count
	return &resp, nil

}
func (d *deliveryTarifRepo) Update(ctx context.Context, request *pbo.DeliveryTarif) (*pbo.DeliveryTarif, error) {
	var (
		delivery = pbo.DeliveryTarif{}
		params   = make(map[string]interface{})
		query    = `update delivery_tarif set `
		filter   = ""
	)

	params["id"] = request.GetId()
	fmt.Println("delivery_tarif_id", request.GetId())

	if request.GetBasePrice() != 0.0 {
		params["base_price"] = request.GetBasePrice()
		filter += " base_price=@base_price,"
	}
	if request.GetName() != "" {
		params["name"] = request.GetName()
		filter += " name=@name,"
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id,client_id,branch_id,type,address,courier_id,price,delivery_price,discount,status,payment_type  `

	fullQuery, args := helper.ReplaceQueryParams(query, params)
	if err := d.db.QueryRow(ctx, fullQuery, args...).Scan(
		&delivery.Id,
		&delivery.Name,
		&delivery.Type,
		&delivery.BasePrice,
	); err != nil {
		d.log.Error("err", logger.Error(err))
		return nil, err
	}

	return &pbo.DeliveryTarif{}, nil

}
func (d *deliveryTarifRepo) Delete(ctx context.Context, request *pbo.PrimaryKeyDelivery) (*emptypb.Empty, error) {

	query := `update delivery_tarif set deleted_at = extract(epoch from current_timestamp) where id = $1`
	_, err := d.db.Exec(ctx, query, request.Id)
	return nil, err
}
