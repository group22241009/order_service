package postgres

import (
	"context"
	"fmt"
	pbo "order_service/genproto/order_service"
	"order_service/pkg/helper"
	"order_service/pkg/logger"
	"order_service/storage"

	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"
)

const (
	OrderStatusAccepted        = "accepted"
	OrderStatusCourierAccepted = "courier_accepted"
	OrderStatusReadyInBranch   = "ready_in_branch"
	OrderStatusOnWay           = "on_way"
	OrderStatusFinished        = "finished"
	OrderStatusCanceled        = "canceled"
)
const (
	OrderTypeDelivery = "delivery"
	OrderTypePickUp   = "pick_up"
)
const (
	PaymentTypeCard = "card"
	PaymentTypeCash = "cash"
)

type orderRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewOrderRepo(db *pgxpool.Pool, log logger.ILogger) storage.IOrdersStorage {
	return &orderRepo{
		db:  db,
		log: log,
	}
}

func (o *orderRepo) Create(ctx context.Context, request *pbo.CreateOrder) (*pbo.Order, error) {
	var (
		orders = pbo.Order{}
		err    error
	)
	query := ` insert into orders(id,client_id,branch_id,type,address,courier_id,price,delivery_price,discount,status,payment_type)
	            values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)
	            returning id, client_id, branch_id,type::text,address,courier_id,price,delivery_price,discount,status::text,payment_type::text `

	if err = o.db.QueryRow(ctx, query, request.Id, request.GetClientId(), request.GetBranchId(), OrderTypeDelivery, request.GetAddress(), request.GetCourierId(), request.GetPrice(),
		request.GetDeliveryPrice(), request.GetDiscount(), OrderStatusAccepted, PaymentTypeCash).Scan(
		&orders.Id,
		&orders.ClientId,
		&orders.BranchId,
		OrderTypePickUp,
		&orders.Address,
		&orders.CourierId,
		&orders.Price,
		&orders.DeliveryPrice,
		&orders.Discount,
		OrderStatusAccepted,
		PaymentTypeCash,
	); err != nil {
		fmt.Println("err", err)
	}
	return &orders, nil
}
func (o *orderRepo) Get(ctx context.Context, request *pbo.PrimaryKeyOrder) (*pbo.Order, error) {
	orders := pbo.Order{}
	query := `select id,client_id,branch_id,type,address,courier_id,price,delivery_price,discount,status,payment_type from orders where deleted_at=0 and id=$1 `
	if err := o.db.QueryRow(ctx, query, request.GetId()).Scan(
		&orders.Id,
		&orders.ClientId,
		&orders.BranchId,
		OrderTypePickUp,
		&orders.Address,
		&orders.CourierId,
		&orders.Price,
		&orders.DeliveryPrice,
		&orders.Discount,
		OrderStatusAccepted,
		PaymentTypeCash,
	); err != nil {
		o.log.Error("error getting orders", logger.Error(err))
		return &pbo.Order{}, err
	}
	return &orders, nil
}
func (o *orderRepo) GetList(ctx context.Context, request *pbo.GetListRequestOrder) (*pbo.OrderListResponse, error) {
	var (
		resp   = pbo.OrderListResponse{}
		filter = " where deleted_at=0"
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)
	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%s'", request.GetSearch())
	}
	countQuery := `select count(*) from orders ` + filter
	if err := o.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		o.log.Error("error while scanning count of orders", logger.Error(err))
		return nil, err
	}

	query := ` select id,client_id,branch_id,type,address,courier_id,price,delivery_price,discount,status,payment_type from orders` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())

	rows, err := o.db.Query(ctx, query)
	if err != nil {
		o.log.Error("error while getting orders rows", logger.Error(err))
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		order := pbo.Order{}

		if err = rows.Scan(
			&order.Id,
			&order.ClientId,
			&order.BranchId,
			OrderTypePickUp,
			&order.Address,
			&order.CourierId,
			&order.Price,
			&order.DeliveryPrice,
			&order.Discount,
			OrderStatusAccepted,
			PaymentTypeCash,
		); err != nil {
			o.log.Error("error getting list of orders", logger.Error(err))
			return &pbo.OrderListResponse{}, err
		}
		resp.Orders = append(resp.Orders, &order)
	}

	resp.Count = count
	return &resp, nil
}

func (o *orderRepo) Update(ctx context.Context, request *pbo.Order) (*pbo.Order, error) {
	var (
		order  = pbo.Order{}
		params = make(map[string]interface{})
		query  = `update orders set `
		filter = ""
	)

	params["id"] = request.GetId()
	fmt.Println("order id", request.GetId())

	if request.GetPrice() != 0.0 {
		params["price"] = request.GetPrice()
		filter += " price = @price,"
	}
	if request.GetAddress() != "" {
		params["address"] = request.GetAddress()
		filter += " address= @address"
	}
	if request.GetBranchId() != "" {
		params["branch_id"] = request.GetBranchId()
		filter += " branch_id= @branch_id"
	}
	if request.GetClientId() != "" {
		params["client_id"] = request.GetClientId()
		filter += " client_id= @client_id"
	}
	if request.GetCourierId() != "" {
		params["courier_id"] = request.GetCourierId()
		filter += " courier_id=@courier_id"
	}
	if request.GetDeliveryPrice() != 0.0 {
		params["delivery_price"] = request.GetDeliveryPrice()
		filter += " delivery_price= @delivery_price"
	}
	if request.GetDiscount() != 0.0 {
		params["discount"] = request.GetDiscount()
		filter += " discount= @discount"
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id,client_id,branch_id,type,address,courier_id,price,delivery_price,discount,status,payment_type  `

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := o.db.QueryRow(ctx, fullQuery, args...).Scan(
		&order.Id,
		&order.ClientId,
		&order.BranchId,
		OrderTypePickUp,
		&order.Address,
		&order.CourierId,
		&order.DeliveryPrice,
		&order.Price,
		&order.Discount,
		OrderStatusAccepted,
		PaymentTypeCash,
	); err != nil {
		return nil, err
	}

	return &pbo.Order{}, nil

}
func (o *orderRepo) Delete(ctx context.Context, request *pbo.PrimaryKeyOrder) (*emptypb.Empty, error) {
	query := `update orders set deleted_at = extract(epoch from current_timestamp) where id = $1`
	_, err := o.db.Exec(ctx, query, request.Id)

	return nil, err
}

func (o *orderRepo) Count(ctx context.Context, e *emptypb.Empty) (*pbo.OrderId, error) {
	count := pbo.OrderId{}

	query := `SELECT id FROM orders ORDER BY id DESC LIMIT 1`

	row := o.db.QueryRow(ctx, query)
	var id string
	err := row.Scan(&id)
	if err != nil {
		o.log.Error("err",logger.Error(err))
	
	}
	count.Id = id
	return &count, nil
}
